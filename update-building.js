const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
    const newInformaticsBuilding = await Building.findById('625b3105c477228c1838b9dc')
    const room = await Room.findById('625b3105c477228c1838b9e1')
    const informaticsBuilding = await Building.findById(room.building)
    console.log(newInformaticsBuilding)
    console.log(room)
    console.log(informaticsBuilding)
    room.building = newInformaticsBuilding
    newInformaticsBuilding.rooms.push(room)
    informaticsBuilding.rooms.pull(room)
    room.save()
    newInformaticsBuilding.save()
    informaticsBuilding.save()
}

main().then(() => {
    console.log('Finish')
})